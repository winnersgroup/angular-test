# Insight

1. Build the Look and Feel of Insight (with provided mocks in assets/mocks) using and Angular and Sass,
also we would like to see some Angular logic, controllers, directives, filters etc.

## Prerequisites

1. [Bower](http://bower.io/)
2. [Angular](https://angular.io/)
3. [Materialize](http://materializecss.com/getting-started.html).

## Build & development

The Main goal is to see your coding and troubleshooting skills.
